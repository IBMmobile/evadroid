VERSION = 1.0
APKS = AbnormalSetting/installedApps AbnormalSetting/uptime AnalysisDetection/adbEnable AnalysisDetection/adbPortDetector AnalysisDetection/signatureVerification EmulatorConstants/constantCalls1 EmulatorConstants/constantCalls2 EmulatorConstants/constants1 EmulatorConstants/constants2 EmulatorConstants/constantsDLC EmulatorConstants/divById EmulatorConstants/qemuFingerprinting Network/getIpAddress Network/procNetTcp Sensors/accelH Sensors/batteryStatus Sensors/batteryCharging Sensors/batteryFull Time/longAction Time/atNight Time/postDelayed Time/sleep

RM	= /bin/rm
CP	= /bin/cp
ECHO	= /bin/echo
ZIP	= /usr/bin/zip
GRADLEW	= ./gradlew
JARSIGNER = /usr/bin/jarsigner
PWD	= `pwd`

all : evadroid-$(VERSION).zip

clean : clean-apks
	$(RM) *.apk
	$(RM) evadroid-$(VERSION).zip

evadroid-$(VERSION).zip : copy-files
	$(ZIP) evadroid-$(VERSION).zip *.apk tests.yml check-test.py README.md

copy-files : sign-apks
	-for d in $(APKS); do ( $(CP) $$d/app/build/outputs/apk/app-release-unsigned.apk $(PWD)/$$(basename $$d).apk ); done

clean-apks : 
	-for d in $(APKS); do ( $(ECHO) $$d; cd $$d; $(RM) app/build/outputs/apk/* ); done

assembleRelease-apks :
	-for d in $(APKS); do ( $(ECHO) $$d; cd $$d; echo "sdk.dir=$(SDKDIR)" > local.properties; ./build.sh $(GRADLEW) $$(basename $$d).apk); done

sign-apks : check-env assembleRelease-apks 
	-for d in $(APKS); do ( $(ECHO) $$d; cd $$d; $(ECHO) $(PASSKEYSTORE) | $(JARSIGNER) -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $(KEYSTORE) app/build/outputs/apk/app-release-unsigned.apk $(ALIASNAME) ); done

check-env:
ifndef KEYSTORE
    $(error KEYSTORE is undefined: please set this env to your keystore file)
endif
ifndef ALIASNAME
    $(error ALIASNAME is undefined: please set this env to your alias name)
endif
ifndef PASSKEYSTORE
    $(error PASSKEYSTORE is undefined: please set this env to your keystore file password)
endif
ifndef SDKDIR
    $(error SDKDIR is undefined: please set this env to the path of the Android SDK)
endif
