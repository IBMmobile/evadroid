# README #

Evadroid is a catalog of techniques that malware can use to evade detection and analysis. Each of the sample apps in this catalog detects a very specific aspect of the running system on which the payload execution depends. [Learn more](http://ibmmobile.bitbucket.org/).

## What are the test apps doing? ##

Each of the apps execute its payload (send SMSs) only under certain conditions. If the payload is triggered, the app toasts a message "Payload triggered!" as well as logging "payload". The conditions to pass the tests are defined in the file ``tests.yml``.

## How do I get set up? ##

* Download the zip file with all the apps from the [website](http://ibmmobile.bitbucket.io/).

* Run each of them in an emulator or in a device.
* Collect the logs. For example ``adb logcat -d > mylog.log``
* You can analyze the logs manually or using ``check-test.py``, which is included.
* ``check-test.py`` needs the case to analyze, for example ``constants1``. Requires Python 3 and Yaml to run. 
* Run ``cat mylog.log |  ./check-test.py  'constants1'`` or ``./check-test.py  -l constant1-success.log constants1``

### Contribution guidelines ###

If you are interested in extend this test suite, [contact us](https://ibm.biz/AresSystem).

### Requirements ###
 * Android SDK android-23
 * Build Tools 23.0.1

### Build the APKs ###
* If you dont have one yet, you have to create a key: https://developer.android.com/studio/publish/app-signing.html#signing-manually
* you need to define env variables with
$ export KEYSTORE=/Users/(...)/my-release-key.keystore // Full path to your keystore
$ export ALIASNAME=alias_name
$ export PASSKEYSTORE=pass

* Define the path location for the SDK directory
$ export SDKDIR="(...)/Library/Android/sdk"
