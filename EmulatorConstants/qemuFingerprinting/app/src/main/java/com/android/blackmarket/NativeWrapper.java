package com.android.blackmarket;

import android.util.Log;

public class NativeWrapper
{
  static
  {
    try {
	  System.loadLibrary("bt_detect2");
    }
    catch (UnsatisfiedLinkError e) {
      Log.e("evadroid","Native code library failed to load" + e);
      System.exit(1);
    }
  }

  public native static int is_synch_proc_switch();
  
  public static int is_in_emu()
  {
    int r = is_synch_proc_switch();

    Log.d("evadroid",String.format("native library called: %s", r));
    return r;
  }
}